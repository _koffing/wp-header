# wp-header #

wp-header is a header plugin that is meant to be used with wordpress. wp-header works along with the wp_nav_menu() function to create a header and nav that is fully responsive. The nav is capable of nesting
to three layers.

### Setup ###

* Copy the html snippet from wp-header.html into the body of your html.
* Include the scss or css into your project.
* Include wp-header.js into your projects JavaScript (requires jQuery as a dependency).

### Dependencies ###

* jQuery
